/* [Shaft] */
diameter = 5;        // [1:0.1:10]
length = 7;          // [1:0.1:20]
vertical_offset = 0; // [0:0.1:20]
edge_radius = 1;     // [0:0.01:10]

/* [Plate] */
head_length = 10;            // [1:0.1:20]
head_width = 7;              // [1:0.1:20]
head_height = 13;            // [1:0.1:20]
plate_support_thickness = 2; // [0.1:0.1:5]

assert(edge_radius < diameter / 2, "edge_radius too large");
assert(vertical_offset < head_height - diameter, "vertical_offset too large");

$fn = 50;

cutout_side = 2 * sqrt(pow(head_height, 2) + pow(head_length, 2));
cutout_angle = atan2(head_height, head_length);

union()
{
  // shaft
  translate([ diameter / 2 + vertical_offset, edge_radius, diameter / 2 ])
    rotate([ 90, 0, 0 ]) render() intersection()
  {
    translate([ 0, 0, (length + edge_radius) / 2 ])
      cube([ diameter, diameter, length + edge_radius ], center = true);
    minkowski()
    {
      cylinder(r = diameter / 2 - edge_radius, h = length);
      sphere(r = edge_radius);
    }
  }

  // head
  render() difference()
  {
    // crude head slab
    cube([ head_height, head_length, head_width ]);
    // remove inners of the head
    translate([
      plate_support_thickness,
      plate_support_thickness,
      plate_support_thickness
    ]) cube([ head_height, head_length, head_width ]);
    // Remove diagonal part
    linear_extrude(head_width) polygon(
      [[head_height * 1.5,
        head_length *
          1.5], // with a little tolerance to get rid of rendering artifacts
       [plate_support_thickness, head_length],
       [head_height, plate_support_thickness]]);
  }
}

